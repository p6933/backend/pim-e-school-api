export class RouteModel {
    constructor(
        public method: string,
        public route: string | undefined,
        public action: string
    ) {}
}

export class RouteList {
    constructor(
        public method: string,
        public route: string ,
        public controller: any,
        public action: string
    ) {}
}