/**
 * Classe représentant un item de l'emploi du temps d'un élève
 * @param {Date} lessonStartDate - Date et heure de début du cours
 * @param {Date} lessonEndDate - Date et heure de fin du cours
 * @param {string} subjectName - Nom du cours
 * @param {string | undefined} subjectDescription - Description du cours
 * @param {string} teacherLastName - Nom du professeur donnant le cours
 * @param {string} teacherFirstName - Prénom du professeur donnant le cours
 * @param {string} classRoomName - Nom de la salle où le cours a lieu
 * @param {string | undefined} classRoomDesc - Description de la salle où le cours a lieu
 */
export class StudentScheduleModel {
    constructor(
        public lessonStartDate: Date,
        public lessonEndDate: Date,
        public subjectName: string,
        public subjectDescription: string | undefined,
        public teacherLastName: string,
        public teacherFirstName: string,
        public classRoomName: string,
        public classRoomDesc: string | undefined
    ) {  }
}

/**
 * Classe contenant les informations personelles publiques de l'élève
 * @param {string} email - Email de l'élève
 * @param {string | undefined} profilePicture - Chemin d'accès à l'image de profil de l'élève
 * @param {string} firstName - Prénom de l'élève
 * @param {string} lastName - Nom de l'élève
 * @param {Date} birthdate - Date de naissance de l'élève
 * @param {string} address - Adresse de l'élève
 * @param {string} phone - Numéro de téléphone de l'élève
 */
export class StudentInfo {
    constructor(
        public email: string,
        public profilePicture: string | undefined,
        public firstName: string,
        public lastName: string,
        public birthdate: Date,
        public address: string,
        public phone: string
    ) {  }
}