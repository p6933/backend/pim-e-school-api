import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import { Request, Response } from "express";
import swaggerUI from "swagger-ui-express";
import swaggerJsDoc from "swagger-jsdoc";

import router, { CreateRoutes } from "./routes/routes";
import { createConnection } from "typeorm";
import adminRouter from "./routes/admin.routes";
import teacherRouter from "./routes/teacher.routes";
import studentRouter from "./routes/student.routes";

const port: number = 9999;
const host: string = "localhost"; // Don't put protocol

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "E-school Swagger Doc",
      version: "1.0.0",
      description: "Documentation for E-school server",
    },
    servers: [
      {
        url: `http://localhost:${port}`,
      },
    ],
  },
  apis: ["./routes/*.ts"],
};

const specs = swaggerJsDoc(options);

/**
 * Connect to database with TypeORM
 * Using ormconfig.json
 */
createConnection()
  .then(() => {
    /**
    * Création du routeur Express
    * @type {object}
    * @const
    */
    const app: express.Application = express();
    app.use(
      cors({
        exposedHeaders: ["Content-Disposition", "X-Header-Filename"],
        allowedHeaders: "*",
        origin: "*",
        methods: "*",
      })
    );
    app.use("/e-school-api-docs", swaggerUI.serve, swaggerUI.setup(specs));
    app.use(bodyParser.json());

      /**
       * Routes pour séparer les appels API.
       * @function
       * @param {string} path - Express path
       * @param {callback} middleware - Express middleware.
       */
    app.use("/api", router);
    app.use("/student", studentRouter);
    app.use("/teacher", teacherRouter);
    app.use("/admin", adminRouter);
    
    app.listen(port, host);
    console.log("Server started at http://%s:%d", host, port);
    
      /**
       * Creation des routes principales et de la liste des tables
       * @const
       *
       */
    const Routes = CreateRoutes();

    /**
     * Generation des routes pour chaque table à partir de l'objet Routes
     */
    Routes.forEach((route) => {
      (app as any)[route.method](
        route.route,
        (req: Request, res: Response, next: Function) => {
          const result = new (route.controller as any)()[route.action](
            req,
            res,
            next
          );
          if (result instanceof Promise) {
            result.then((result) =>
              result !== null && result !== undefined
                ? res.send(result)
                : undefined
            );
            console.log(result);
          } else if (result !== null && result !== undefined) {
            res.json(result);
            console.log(result);
          }
        }
      );
    });
  })
  .catch((error) => console.log(error));
