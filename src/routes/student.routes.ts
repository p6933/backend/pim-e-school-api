import express from 'express';
import {StudentController} from "../controllers/Student.controller";
import {Request, Response} from 'express';

const studentRouter = express.Router({
    caseSensitive: true
});

// Route de connexion Student
studentRouter.post('/connect', async (req: Request, res: Response) => {
    const student = new StudentController;
    const logged = await student.connect(req.body.email, req.body.password);
    await res.status(200).json({
        status: 200,
        logged: logged
    })
})

//Route pour obtenir les infos perso de l'élève
studentRouter.post('/info', async (req, res) => {
    const student = new StudentController();
    const email: string = req.body.email;
    const debug: boolean = req.body.debug;
    const studentInfo = await student.getInfo(email, debug);
    if (await studentInfo instanceof Error) {
        res.status(500).json({
            "message": studentInfo.toString(),
        });
    } else {
        await res.status(200).json(
            studentInfo
        );
    }
})

/**
 * Route pour obtenir l'emploi du temps de l'élève
 * @function
 * @param {string} path - route de la requête
 * @param {callback} middleware - fonction middleware
 */
// Route emploi du temps Student
studentRouter.post('/schedule', async (req: Request, res: Response) => {
    const student = new StudentController;
    const startDate: Date = req.body.startDate;
    const endDate: Date = req.body.endDate;
    const email: string = req.body.email;
    const year: number = req.body.year;
    const debug: boolean = req.body.debug;
    const schedule = await student.getSchedule(startDate, endDate, email, year, debug);
    if (await schedule instanceof Error) {
        res.status(500).json({
            "message": schedule.toString(),
        });
    } else {
        await res.status(200).json({
            schedule
        })
    }
})

export default studentRouter;