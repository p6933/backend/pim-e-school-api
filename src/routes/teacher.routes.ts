import express from 'express';
import {Request, Response} from 'express';
import {TeacherController} from "../controllers/Teacher.controller";

const teacherRouter = express.Router({
    caseSensitive: true
});

// Route de connexion Teacher
teacherRouter.post('/connect', async (req: Request, res: Response) => {
    const teacher = new TeacherController;
    const logged = await teacher.connect(req.body.email, req.body.password);
    await res.status(200).json({
        status: 200,
        logged: logged
    })
})

export default teacherRouter;