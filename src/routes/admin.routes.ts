import express from 'express';
import {Request, Response} from 'express';
import {AdminController} from "../controllers/Admin.controller";

const adminRouter = express.Router({
    caseSensitive: true
});

// Route de connexion Admin
adminRouter.post('/connect', async (req: Request, res: Response) => {
    const admin = new AdminController;
    const logged = await admin.connect(req.body.email, req.body.password);
    await res.status(200).json({
        status: 200,
        logged: logged
    })
})

export default adminRouter;