import express from 'express';
import {NextFunction, Request, Response} from 'express';
import {UserController} from "../controllers/User.controller";
import {RouteList, RouteModel} from "../models/Route.model";
import {EvaluationController} from "../controllers/Evaluation.controller";
import {InscriptionController} from "../controllers/Inscription.controller";
import {LessonController} from "../controllers/Lesson.controller";
import {PresenceController} from "../controllers/Presence.controller";
import {SchoolClassController} from "../controllers/SchoolClass.controller";
import {ScoreController} from "../controllers/Score.controller";
import {StudentController} from "../controllers/Student.controller";
import {SubjectController} from "../controllers/Subject.controller";
import {TeacherController} from "../controllers/Teacher.controller";
import {TeachingController} from "../controllers/Teaching.controller";
import {AdminController} from "../controllers/Admin.controller";
import {ClassroomController} from "../controllers/Classroom.controller";

// Liste des routes qui representent les methodes HTTP
export const Routes: Array<RouteModel> = [{
    method: "get",
    route: "",
    action: "all"
}, {
    method: "get",
    route: "/:id",
    action: "one"
}, {
    method: "post",
    route: "",
    action: "save"
}, {
    method: "post",
    route: "/:id",
    action: "update"
}, {
    method: "delete",
    route: "/:id",
    action: "remove"
}];

// Création des routes
export function CreateRoutes() {
    
    // Liste des tables de la bdd
    const controllers: Array<any> = [
        AdminController,
        EvaluationController,
        InscriptionController,
        LessonController,
        PresenceController,
        SchoolClassController,
        ScoreController,
        StudentController,
        SubjectController,
        TeacherController,
        TeachingController,
        UserController,
        ClassroomController
    ];

    // Initialisation des tables et création dans la bdd
    let controllersInstances: Array<any> = [];
    controllers.forEach(controller => {
        controllersInstances.push(new controller())
    });
    
    // Association de chaque table avec son url
    let controllersUrls: Array<any> = [];
    controllersInstances.forEach(controller => {
        controllersUrls.push({
            "url" : controller.url,
            "tableName": controller.tableName
        });
    });
    
    // Route pour retirer la liste des routes de la bdd depuis l'exterieur
    router.get('/getRoutes', ((req, res) => {
        console.log('getRoutes : ', controllersUrls)
        res.send(controllersUrls)
    }));
    
    // Liste des url de tables pour lesquelles il faut créer les routes GET/UPDATE/DELETE
    let RoutesList: Array<RouteList> = [];
    for(let i = 0; i< controllers.length; i++) {
        let controller = controllers[i];
        let url = controllersUrls[i].url;
        Routes.forEach( route => {
            RoutesList.push({
                method: route.method,
                route: '/' + url + route.route,
                controller: controller,
                action: route.action
            });
        });
    }
    console.log('Routes created')
    
    return RoutesList
}

// Parametre du routeur
const router = express.Router({
    caseSensitive: true
});

// Route de test racine
router.get('/', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({
        status: 200,
        message: "Router working"
    });
    next();
});

// Route de test api
router.get('/test', (req: Request, res: Response, next: NextFunction) => {
    res.status(200).json({
        status: 200,
        message: "API working"
    });
    next();
});

export default router;
