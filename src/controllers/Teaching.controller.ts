import {UniversalController} from "./Universal.controller";
import {TeachingEntity} from "../entity/Teaching.entity";

export class TeachingController extends UniversalController {
    constructor() {
        super(
            TeachingEntity,
            'teaching'
        )
    }
}