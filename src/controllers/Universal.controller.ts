import {Connection, EntityTarget, getConnection, getRepository, Repository} from "typeorm";
import {NextFunction, Request, Response} from "express";

export abstract class UniversalController {
    protected constructor(
        private entite: EntityTarget<any>,
        public url: string
    ) {
        this.getTableName();
        console.log(`Nom de la table utilisée : ${this.tableName}`);
    }
    public tableName: string = "NoDefinedName";
    private connection: Connection = getConnection();
    protected repository: Repository<any> = getRepository(this.entite);
    
    public getTableName() {
        const result = this.connection.getMetadata(this.entite).givenTableName;
        if (result !== undefined) {
            this.tableName = result;
        }
    }
    
    async all(req: Request, res: Response, next: NextFunction) {
        return this.repository.find();
    }
    
    async one(req: Request, res: Response, next: NextFunction) {
        return this.repository.findOne(req.params.id)
            .catch((error: any) => {
                console.log(error)
                return error
            })
    }
    
    async save(req: Request, res: Response, next: NextFunction) {
        const result = await this.repository.save(req.body)
            .catch((error: any) => {
                console.log(error)
                return error
            })
        
        console.log("Body : ",req.body)
        
        return result
    }
    
    async update(req: Request, res: Response, next: NextFunction) {
        return await this.repository.update(req.params.id, req.body)
            .catch((error: any) => {
                console.log(error)
                return error
            })
    }
    
    async remove(req: Request, res: Response, next: NextFunction) {
        let toRemove = await this.repository.findOne(req.params.id);
        if ( typeof toRemove !== 'undefined' ) {
            await this.repository.remove(toRemove)
                .catch((error: any) => {
                console.log(error);
                return error;
            });
            return 'Objet supprimé';
        } else {
            let err = 'Objet à supprimer non trouvé';
            console.log(err);
            return err
        }
    }
    
}