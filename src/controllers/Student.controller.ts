import {UniversalController} from "./Universal.controller";
import {StudentEntity} from "../entity/Student.entity";
import { getRepository } from "typeorm";
import {InscriptionEntity} from "../entity/Inscription.entity";
import {StudentInfo, StudentScheduleModel} from "../models/Student.model";

export class StudentController extends UniversalController {
    constructor() {
        super(
            StudentEntity,
            'student'
        )
    }
    
    /**
     * Récupérer le mot de passe correspondant au mail puis le comparer au mot de passe fourni
     * @async
     * @param {string} email - Email de l'élève pour l'identification
     * @param {string} password - Mot de passe pour l'authentification
     * @return {boolean} -- Renvoie true si la connexion est acceptée
     */
    async connect(email: string, password: string): Promise<boolean> {
        const credentials = await getRepository(StudentEntity)
            .createQueryBuilder("student")
            .where("student.email = :email", {email: email})
            .getOneOrFail();
        
        return credentials?.password == password;
    }
    
    /**
     * Récupère les informations personelles de l'élève
     * @async
     * @param {string} email - Email de l'élève pour l'identification
     * @param {boolean} debug - Afficher les données brutes
     * @return {StudentInfo | StudentEntity | Error} -- Informations personnelles de l'élève
     */
    async getInfo(email: string, debug: boolean): Promise<StudentInfo | StudentEntity | Error> {
        const response = await getRepository(StudentEntity)
            .createQueryBuilder("student")
            .leftJoinAndSelect("student.user", "user")
            .where("student.email = :email", {email: email})
            .getOne();
    
        console.log("Response : ");
        console.log(response);
        
        if (!response) {
            return new Error("Student not found");
        }
        
        let studentInfo: StudentInfo | null = null;
        if (
            response.email
            && response.user?.firstName
            && response.user?.lastName
            && response.user?.birthdate
            && response.user?.address
            && response.user?.phone
        ) {
            studentInfo = {
                email: response.email,
                profilePicture: response.profilePicture,
                firstName: response.user.firstName,
                lastName: response.user.lastName,
                birthdate: response.user.birthdate,
                address: response.user.address,
                phone: response.user.phone,
            }
        }
        
        console.log("Student info : ");
        console.log(studentInfo);
        
        if (!studentInfo) {
            throw new Error("Student info incomplete")
        } else if (!debug) {
            return studentInfo
        } else {
            return response
        }
    }
    
    /**
     *  Récupère l'emploi du temps de l'élève en fonction de la période de temps donnée
     *  @async
     *
     * @param {Date} startDate - Date de début de la période de récupération des cours
     * @param {Date} endDate - Date de fin de la période de récupération des cours
     * @param {string} email - Email de l'élève permettant l'identification
     * @param {number} year - Année de l'inscription de l'élève
     * @param {boolean} debug - Afficher les données brutes
     * @return {StudentScheduleModel[] | InscriptionEntity[] | Error} -- Emploi du temps de l'élève
     */
    async getSchedule(startDate: Date, endDate: Date, email: string, year: number, debug: boolean): Promise<StudentScheduleModel[] | InscriptionEntity[] | Error> {
        const yearStart: Date = new Date(year, 0, 1);
        const yearEnd: Date = new Date(year, 12, 31);
        const response: InscriptionEntity[] = await getRepository(InscriptionEntity)
            .createQueryBuilder("inscription")
            .leftJoinAndSelect("inscription.student", "student")
            .leftJoinAndSelect("inscription.schoolClass", "schoolClass")
            .leftJoinAndSelect("schoolClass.lessons", "lessons")
            .leftJoinAndSelect("lessons.subject","subject")
            .leftJoinAndSelect("lessons.teaching","teaching")
            .leftJoinAndSelect("lessons.classRoom", "classRoom")
            .leftJoinAndSelect("teaching.teacher","teacher")
            .leftJoinAndSelect("teacher.user", "user")
            .where("student.email = :email", {email: email})
            .andWhere("inscription.startDate > :yearStart", {yearStart: yearStart})
            .andWhere("inscription.startDate < :yearEnd", {yearEnd: yearEnd})
            .andWhere("lessons.startDate > :startDate", {startDate: startDate})
            .andWhere("lessons.endDate < :endDate", {endDate: endDate})
            .getMany();
    
        console.log("Response : ");
        console.log(response);
        
        if (!response[0]) {
            return new Error("Student not found");
        }
        
        let schedule: Array<StudentScheduleModel> = [];
        response[0].schoolClass?.lessons?.forEach( (lesson) => {
            if (
                lesson.startDate
                && lesson.endDate
                && lesson.subject?.name
                && lesson.subject?.description
                && lesson.teaching?.teacher?.user?.lastName
                && lesson.teaching?.teacher?.user?.firstName
                && lesson.classRoom?.name
                && lesson.classRoom?.description
            ) {
                schedule.push({
                    "lessonStartDate": lesson.startDate,
                    "lessonEndDate": lesson.endDate,
                    "subjectName": lesson.subject?.name,
                    "subjectDescription": lesson.subject?.description,
                    "teacherLastName": lesson.teaching?.teacher?.user?.lastName,
                    "teacherFirstName": lesson.teaching?.teacher?.user?.firstName,
                    "classRoomName": lesson.classRoom?.name,
                    "classRoomDesc": lesson.classRoom?.description
                });
            }
        });
        
        console.log("Schedule : ");
        console.log(schedule);
        if (!debug) {
            return schedule
        } else {
            return response
        }
        
    }
}