import {UniversalController} from "./Universal.controller";
import {TeacherEntity} from "../entity/Teacher.entity";
import {getRepository} from "typeorm";

export class TeacherController extends UniversalController {
    constructor() {
        super(
            TeacherEntity,
            'teacher'
        )
    }
    
    // Récuperer le mot de passe correspondant au mail puis le comparer au mot de passe fourni
    async connect(email: string, password: string): Promise<boolean> {
        const credentials = await getRepository(TeacherEntity)
            .createQueryBuilder("teacher")
            .where("teacher.email = :email", {email: email})
            .getOne();
        
        return credentials?.password == password;
    }
}