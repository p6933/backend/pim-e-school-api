import {UniversalController} from "./Universal.controller";
import {InscriptionEntity} from "../entity/Inscription.entity";

export class InscriptionController extends UniversalController {
    constructor() {
        super(
            InscriptionEntity,
            'inscription'
        );
    }
}