import {UniversalController} from "./Universal.controller";
import {ScoreEntity} from "../entity/Score.entity";

export class ScoreController extends UniversalController {
    constructor() {
        super(
            ScoreEntity,
            'score'
        )
    }
}