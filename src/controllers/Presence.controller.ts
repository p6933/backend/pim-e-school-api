import {UniversalController} from "./Universal.controller";
import {PresenceEntity} from "../entity/Presence.entity";

export class PresenceController extends UniversalController {
    constructor() {
        super(
            PresenceEntity,
            'presence'
        )
    }
}