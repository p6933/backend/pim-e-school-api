import {UniversalController} from "./Universal.controller";
import {SchoolClassEntity} from "../entity/SchoolClass.entity";

export class SchoolClassController extends UniversalController {
    constructor() {
        super(
            SchoolClassEntity,
            'schoolclass'
        )
    }
}