import {UniversalController} from "./Universal.controller";
import {ClassroomEntity} from "../entity/Classroom.entity";

export class ClassroomController extends UniversalController {
    constructor() {
        super(
            ClassroomEntity,
            'classroom'
        )
    }
}