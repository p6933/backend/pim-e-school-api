import {UniversalController} from "./Universal.controller";
import {EvaluationEntity} from "../entity/Evaluation.entity";

export class EvaluationController extends UniversalController {
    constructor() {
        super(
            EvaluationEntity,
            'evaluations');
    }
}