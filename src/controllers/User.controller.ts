import {UniversalController} from "./Universal.controller";
import {UserEntity} from "../entity/User.entity";

export class UserController extends UniversalController {
    constructor() {
        super(
            UserEntity,
            'users');
    }
    
}
