import {UniversalController} from "./Universal.controller";
import {SubjectEntity} from "../entity/Subject.entity";

export class SubjectController extends UniversalController {
    constructor() {
        super(
            SubjectEntity,
            'subject'
        )
    }
}