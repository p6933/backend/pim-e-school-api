import {UniversalController} from "./Universal.controller";
import {LessonEntity} from "../entity/Lesson.entity";

export class LessonController extends UniversalController {
    constructor() {
        super(
            LessonEntity,
            'lesson'
        )
    }
}