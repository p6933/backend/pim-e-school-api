import {UniversalController} from "./Universal.controller";
import { getRepository } from "typeorm";
import {AdminEntity} from "../entity/Admin.entity";

export class AdminController extends UniversalController {
    constructor() {
        super(
            AdminEntity,
            'admin'
        )
    }
    
    // Récuperer le mot de passe correspondant au mail puis le comparer au mot de passe fourni
    async connect(email: string, password: string): Promise<boolean> {
        const credentials = await getRepository(AdminEntity)
            .createQueryBuilder("admin")
            .where("admin.email = :email", {email: email})
            .getOne();
        
        return credentials?.password == password;
    }
}