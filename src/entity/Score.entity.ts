import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {StudentEntity} from "./Student.entity";
import { EvaluationEntity } from "./Evaluation.entity";

@Entity('Score')
export class ScoreEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "boolean",
        nullable: false
    })
    isAbsent?: boolean;
    
    @ManyToOne(
        () => EvaluationEntity,
        evaluation => evaluation.scores
    )
    evaluation?: EvaluationEntity
    
    @ManyToOne(
        () => StudentEntity,
        student => student.scores
    )
    student?: StudentEntity
}