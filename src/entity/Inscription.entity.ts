import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm";
import {StudentEntity} from "./Student.entity";
import {SchoolClassEntity} from "./SchoolClass.entity";
import {PresenceEntity} from "./Presence.entity";

@Entity('Inscription')
export class InscriptionEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    startDate?: Date;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    endDate?: Date;
    
    @ManyToOne(
        () => SchoolClassEntity,
        schoolClass => schoolClass.inscriptions
    )
    schoolClass?: SchoolClassEntity
    
    @ManyToOne(
        () => StudentEntity,
        student => student.inscriptions
    )
    student?: StudentEntity
    
    @OneToMany(
        () => PresenceEntity,
        presences => presences.inscription
    )
    presences?: PresenceEntity[]
}