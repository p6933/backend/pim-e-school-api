import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {SchoolClassEntity} from "./SchoolClass.entity";
import {SubjectEntity} from "./Subject.entity";
import {ScoreEntity} from "./Score.entity";
import {TeachingEntity} from "./Teaching.entity";
import {ClassroomEntity} from "./Classroom.entity";

@Entity('Evaluation')
export class EvaluationEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    startDate?: Date;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    endDate?: Date;
    
    @ManyToOne(
        () => SchoolClassEntity,
        schoolClass => schoolClass.evaluations
    )
    schoolClass?: SchoolClassEntity
    
    @ManyToOne(
        () => SubjectEntity,
        subject => subject.evaluations
    )
    subject?: SubjectEntity
    
    @ManyToOne(
        () => ClassroomEntity,
        classroom => classroom.evaluations
    )
    classRoom?: ClassroomEntity
    
    @ManyToOne(
        () => TeachingEntity,
        teaching => teaching.evaluations
    )
    teaching?: TeachingEntity
    
    @OneToMany(
        () => ScoreEntity,
        scores => scores.evaluation
    )
    scores?: ScoreEntity[]
}