import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {InscriptionEntity} from "./Inscription.entity";
import {LessonEntity} from "./Lesson.entity";

@Entity('Presence')
export class PresenceEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "boolean",
        nullable: false
    })
    isPresent?: boolean;
    
    @ManyToOne(
        () => InscriptionEntity,
        inscription => inscription.presences
    )
    inscription?: InscriptionEntity
    
    @ManyToOne(
        () => LessonEntity,
        lesson => lesson.presences
    )
    lesson?: LessonEntity
}