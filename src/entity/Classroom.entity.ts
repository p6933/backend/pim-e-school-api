import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {LessonEntity} from "./Lesson.entity";
import {EvaluationEntity} from "./Evaluation.entity";

@Entity('Classroom')
export class ClassroomEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    name?: string;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    description?: string;
    
    @OneToMany(
        () => LessonEntity,
        lessons => lessons.classRoom
    )
    lessons?: LessonEntity[]
    
    @OneToMany(
        () => EvaluationEntity,
        evaluations => evaluations.classRoom
    )
    evaluations?: EvaluationEntity[]
}