import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm";
import {TeacherEntity} from "./Teacher.entity";
import {SubjectEntity} from "./Subject.entity";
import {EvaluationEntity} from "./Evaluation.entity";
import {LessonEntity} from "./Lesson.entity";

@Entity('Teaching')
export class TeachingEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    startDate?: Date;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    endDate?: Date;
    
    @ManyToOne(
        () => TeacherEntity,
        teacher => teacher.teachings
    )
    teacher?: TeacherEntity
    
    @ManyToOne(
        () => SubjectEntity,
        subject => subject.teachings
    )
    subject?: SubjectEntity
    
    @OneToMany(
        () => LessonEntity,
        lessons => lessons.teaching
    )
    lessons?: LessonEntity[]
    
    @OneToMany(
        () => EvaluationEntity,
        evaluations => evaluations.teaching
    )
    evaluations?: EvaluationEntity[]
}