import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {UserEntity} from "./User.entity";

@Entity('Admin')
export class AdminEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false,
        unique: true
    })
    email?: string;
    
    @Column({
        type : "varchar",
        nullable: false
    })
    password?: string;
    
    @Column({
        type: "varchar",
        nullable: true
    })
    profilePicture?: string
    
    @ManyToOne(
        () => UserEntity,
        user => user.admins
    )
    user?: UserEntity
}