import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne} from "typeorm";
import { TeachingEntity } from "./Teaching.entity";
import {UserEntity} from "./User.entity";

@Entity('Teacher')
export class TeacherEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false,
        unique: true
    })
    email?: string;
    
    @Column({
        type : "varchar",
        nullable: false})
    password?: string;
    
    @Column({
        type: "varchar",
        nullable: true})
    profilePicture?: string
    
    @ManyToOne(
        () => UserEntity,
        user => user.teachers
    )
    user?: UserEntity
    
    @OneToMany(
        () => TeachingEntity,
        teachings => teachings.teacher
    )
    teachings?: TeachingEntity[]
}