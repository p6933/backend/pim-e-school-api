import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {LessonEntity} from "./Lesson.entity";
import {EvaluationEntity} from "./Evaluation.entity";
import {TeachingEntity} from "./Teaching.entity";

@Entity('Subject')
export class SubjectEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    name?: string;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    description?: string;
    
    @OneToMany(
        () => LessonEntity,
        lessons => lessons.subject
    )
    lessons?: LessonEntity[]
    
    @OneToMany(
        () => EvaluationEntity,
        evaluations => evaluations.subject
    )
    evaluations?: EvaluationEntity[]
    
    @OneToMany(
        () => TeachingEntity,
        teachings => teachings.subject
    )
    teachings?: TeachingEntity[]
}