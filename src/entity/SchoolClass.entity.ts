import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {InscriptionEntity} from "./Inscription.entity";
import {LessonEntity} from "./Lesson.entity";
import {EvaluationEntity} from "./Evaluation.entity";

@Entity('SchoolClass')
export class SchoolClassEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    name?: string;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    description?: string;
    
    @OneToMany(
        () => InscriptionEntity,
        inscriptions => inscriptions.schoolClass
    )
    inscriptions?: InscriptionEntity[]
    
    @OneToMany(
        () => LessonEntity,
        lessons => lessons.schoolClass
    )
    lessons?: LessonEntity[]
    
    @OneToMany(
        () => EvaluationEntity,
        evaluations => evaluations.schoolClass
    )
    evaluations?: EvaluationEntity[]
}