import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {SchoolClassEntity} from "./SchoolClass.entity";
import {SubjectEntity} from "./Subject.entity";
import {PresenceEntity} from "./Presence.entity";
import {TeachingEntity} from "./Teaching.entity";
import {ClassroomEntity} from "./Classroom.entity";

@Entity('Lesson')
export class LessonEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    startDate?: Date;
    
    @Column({
        type: "timestamp with time zone",
        nullable: false
    })
    endDate?: Date;
    
    @ManyToOne(
        () => SchoolClassEntity,
        schoolClass => schoolClass.lessons
    )
    schoolClass?: SchoolClassEntity
    
    @ManyToOne(
        () => SubjectEntity,
        subject => subject.lessons
    )
    subject?: SubjectEntity
    
    @ManyToOne(
        () => ClassroomEntity,
        classroom => classroom.lessons
    )
    classRoom?: ClassroomEntity
    
    @ManyToOne(
        () => TeachingEntity,
        teaching => teaching.lessons
    )
    teaching?: TeachingEntity
    
    @OneToMany(
        () => PresenceEntity,
        presences => presences.lesson
    )
    presences?: PresenceEntity[]
}