import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import {StudentEntity} from "./Student.entity";
import {AdminEntity} from "./Admin.entity";
import {TeacherEntity} from "./Teacher.entity";

@Entity('User')
export class UserEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    firstName?: string;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    lastName?: string;
    
    @Column({
        type : "date",
        nullable: false
    })
    birthdate?: Date;
    
    @Column({
        type : "varchar",
        nullable: false
    })
    address?: string;
    
    @Column({
        type: "varchar",
        nullable: false
    })
    phone?: string
    
    @OneToMany(
        () => StudentEntity,
        students => students.user
    )
    students?: StudentEntity[]
    
    @OneToMany(
        () => TeacherEntity,
        teachers => teachers.user
    )
    teachers?: TeacherEntity[]
    
    @OneToMany(
        () => AdminEntity,
        admins => admins.user
    )
    admins?: AdminEntity[]
}