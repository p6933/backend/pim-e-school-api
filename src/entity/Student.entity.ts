import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import {UserEntity} from "./User.entity";
import {ScoreEntity} from "./Score.entity";
import {InscriptionEntity} from "./Inscription.entity";

@Entity('Student')
export class StudentEntity {
    
    @PrimaryGeneratedColumn()
    id?: number;
    
    @Column({
        type: "varchar",
        nullable: false,
        unique: true
    })
    email?: string;
    
    @Column({
        type : "varchar",
        nullable: false})
    password?: string;
    
    @Column({
        type: "varchar",
        nullable: true})
    profilePicture?: string
    
    @ManyToOne(
        () => UserEntity,
        user => user.students
    )
    user?: UserEntity
    
    @OneToMany(
        () => InscriptionEntity,
        inscriptions => inscriptions.student
    )
    inscriptions?: InscriptionEntity[]
    
    @OneToMany(
        () => ScoreEntity,
        scores => scores.student
    )
    scores?: ScoreEntity[]
}